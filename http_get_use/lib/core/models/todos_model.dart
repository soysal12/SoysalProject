class TodosModel {
  final String? title;
  final bool? completed;

  TodosModel({this.title, this.completed});

  factory TodosModel.fromJson(Map<String, dynamic> json) {
    return TodosModel(
      title: json['title'],
      completed: json['completed'],
    );
  }
}
