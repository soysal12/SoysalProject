import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_leran/screens/todos_view.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'GETX  Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: Colors.orange[100],
        appBarTheme: const AppBarTheme(elevation: 0.0),
      ),
      home: const TodosView(),
    );
  }
}
