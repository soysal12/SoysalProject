// ignore_for_file: unused_local_variable

import 'package:getx_leran/constants/constants.dart';
import 'package:getx_leran/models/todo.dart';
import 'package:getx_leran/repository/repository.dart';
import 'package:http/http.dart ' as http;

import 'dart:convert';

class TodoRepository implements Repository {
  @override
  Future<String> deleteCompleted(Todo todo) {
    throw UnimplementedError();
  }

  @override
  Future getTodoList() async {
    List todoList = [];
    var url = Uri.parse("${Constants.dataUrl}/todos");

    var response = await http.get(url);

    var jsoncevir = jsonDecode(response.body);

    todoList = jsoncevir.map((t) => Todo.fromJson(t)).toList();

    // for (var i = 0; i < body.length; i++) {
    //   todoList.add(Todo.fromJson(body[i]));
    // }
    return todoList;
  }

  @override
  Future<String> patchCompleted(Todo todo) {
    throw UnimplementedError();
  }

  @override
  Future<String> postCompleted(Todo todo) {
    throw UnimplementedError();
  }

  @override
  Future<String> putCompleted(Todo todo) {
    throw UnimplementedError();
  }
}
