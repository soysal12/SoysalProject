import 'package:getx_leran/models/todo.dart';
import 'package:getx_leran/repository/repository.dart';

class TodoController {
  final Repository _repository;

  TodoController(this._repository);

  Future fecthTodoList() async {
    return _repository.getTodoList();
  }
}
