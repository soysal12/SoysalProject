import 'package:get/get.dart';
import 'package:http_get_use/core/service/todos_service.dart';

class TodosController extends GetxController {
  final TodosApiService service;

  TodosController(this.service);

  Future fecthTodoList() async {
    return service.getTodosList();
  }
}
