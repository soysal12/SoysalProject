import 'package:flutter/material.dart';
import 'package:getx_leran/controller/todo_controller.dart';
import 'package:getx_leran/repository/todo_repository.dart';

class TodosView extends StatelessWidget {
  const TodosView({super.key});

  @override
  Widget build(BuildContext context) {
    var todoController = TodoController(TodoRepository());

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("REST APİ"),
      ),
      body: FutureBuilder(
        future: todoController.fecthTodoList(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          }

          if (snapshot.hasError) {
            return const Center(
              child: Text("error"),
            );
          }

          return ListView.separated(
            itemCount: snapshot.data?.length ?? 0,
            itemBuilder: (context, index) {
              var todo = snapshot.data?[index];
              return Card(
                  //margin: const EdgeInsets.only(top: 10),
                  child: ListTile(
                title: Text(todo?.title ?? "title"),
                subtitle: Text(todo?.completed.toString() ?? "completed"),
              ));
            },
            separatorBuilder: (context, index) {
              return const Divider(
                thickness: 0.5,
                height: 0.5,
              );
            },
          );
        },
      ),
    );
  }
}
