//  FAke Server

import 'package:getx_leran/models/todo.dart';

abstract class Repository {
  // get
  Future getTodoList();
  //patch

  Future<String> patchCompleted(Todo todo);
  //put
  Future<String> putCompleted(Todo todo);
  //delete
  Future<String> deleteCompleted(Todo todo);
  //post
  Future<String> postCompleted(Todo todo);
}
